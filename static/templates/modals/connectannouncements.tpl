<div>
	<form role="form" class="connectannouncements_topic_frm">
    <div>
      <label for="label_dataInizio">Data Inizio</label>
      <input type="date" name="date_start" id="date_start" value="{connectannouncementsData.date_start}">
      <label for="label_dataFine">Data Fine</label>
      <input type="date" name="date_end" id="date_end" value="{connectannouncementsData.date_end}">
    </div>
    <hr />
		<div>
      <div class="text-center"><button type="button" class="selectAll btn btn-xs btn-primary">Seleziona Tutti</button> <button type="button" class="deselectAll btn btn-xs btn-primary">Deseleziona Tutti</button></div>
      <hr />
      <ul class="connect-announcement-categories-list row">
        <!-- BEGIN connectannouncementsData.categories -->
        <li class="col-md-4">
          <input type="checkbox" id="connectannouncements_categories_{connectannouncementsData.categories.cid}" name="connectannouncements_categories" value="{connectannouncementsData.categories.cid}" <!-- IF connectannouncementsData.categories.announcement -->checked<!-- ENDIF connectannouncementsData.categories.announcement --> />
          <label for="connectannouncements_categories_{connectannouncementsData.categories.cid}">{connectannouncementsData.categories.name}</label>
        </li>
        <!-- END connectannouncementsData.categories -->
      </ul>
    </div>
	</form>
</div>